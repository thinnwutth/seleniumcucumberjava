Feature: Login


  @Automate @JIRA-YMQA-12 @JIRA-YMQA-65 @JIRA-YMQA-67 @JIRA-YMQA-69 @JIRA-YMQA-70
  Scenario Outline: Verify login functionality (<hiptest-uid>) (<hiptest-uid>)
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: "<username>"
    And enter password: "<password>"
    And click Sign In
    Then Login successful

    Examples:
      | username | password | hiptest-uid |
      | thinnwutth@gmail.com | C@rpit19971446 | uid:07d7dee7-f748-4af2-b2df-959d10b383a1 |

