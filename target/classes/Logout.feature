Feature: Logout


  Scenario Outline: Verify logout functionality (<hiptest-uid>)
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: "<username>"
    And enter password: "<password>"
    And click Sign In
    Then Login successful
    When click my profile icon
    And click sign out button
    Then Logout successful

    Examples:
      | username | password | hiptest-uid |
      | thinwutthmone@yomabank.com | f+1UzUFrqtBsUATFZntgrw== | uid:b6100723-3649-48d2-a7b8-e7ad192bb3ac |

  @Automate @JIRA-YMQA-12 @JIRA-YMQA-65 @JIRA-YMQA-67 @JIRA-YMQA-69 @JIRA-YMQA-70
  Scenario Outline: Verify login functionality (<hiptest-uid>)
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: "<username>"
    And enter password: "<password>"
    And click Sign In
    Then Login successful

    Examples:
      | username | password | hiptest-uid |
      | thinnwutth@gmail.com | f+1UzUFrqtBsUATFZntgrw== | uid:a6033c38-3b93-4a76-88a7-d8729977dad5 |
