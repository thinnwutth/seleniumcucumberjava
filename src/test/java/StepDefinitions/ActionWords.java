package StepDefinitions;

//import config.PropertiesFile;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class ActionWords {
    public static String browser;
    static WebDriver driver;
    Duration timeout;
    String password;
    public static void main(String[] args) throws IOException {

    }
    public static void setBrowser(){

        browser="Chrome";
    }
    @Before
    public void setBrowserConfig() {
        //no need to use the long path
        String projectLocation = System.getProperty("user.dir");
        System.out.println("Project Location" + projectLocation);
        System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_linux64/chromedriver");
        //System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_win32/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--star-maximized");
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        //options.addArguments("window-size=1920x1080");
        options.addArguments("--window-size=1920,1080");
        driver = new ChromeDriver(options);
        //driver.manage().window().maximize();
        System.out.println("success maximize");
    }
    public static void runTest(){

        driver.get("https://github.com/");

    }

    public static void launchGitHubWebbasedApplication() throws IOException {
        //setBrowser();
        //PropertiesFile.readPropertiesFile();
        //setBrowserConfig();
        runTest();
        //PropertiesFile.writePropertiesFile();
    }

    public void loginSuccessful() {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@class='avatar avatar-small circle']")));
        Assert.assertTrue(element.isDisplayed());
    }

    public void clickSignIn() {
        WebElement clickable =  driver.findElement(By.xpath("//input[@type='submit']"));
        clickable.click();
    }
    public void clickSignInFromMenu() {
        //setWait();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'Sign in')]")));
        WebElement clickable =  driver.findElement(By.xpath("//a[contains(text(),'Sign in')]"));
        //new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Sign in')]"))).click()
        clickable.click();
    }
    public void setWait(){
        timeout = Duration.ofSeconds(10);

    }
    public void enterUsernameP1(String username) {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='login_field']")));
        element.sendKeys(username);
    }

    public void enterPasswordP1(String password) {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='password']")));
        element.sendKeys(password);
    }
    public void clickMyProfileIcon() {

    }

    public void clickSignOutButton() {

    }

    public void logoutSuccessful() {

    }

    @After
    public static void tearDown(){
        //driver.close();;
        driver.quit();
    }

}


