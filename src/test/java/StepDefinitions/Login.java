package StepDefinitions;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

public class Login{
    public ActionWords actionwords = new ActionWords();

    @Given("launch GitHub web-based application")
    public void launchGitHubWebbasedApplication() throws IOException {
        actionwords.launchGitHubWebbasedApplication();
    }

    @Then("Login successful")
    public void loginSuccessful() {
        actionwords.loginSuccessful();
    }

    @When("click Sign In")
    public void clickSignIn() {
        actionwords.clickSignIn();
    }

    @When("click Sign In from menu")
    public void clickSignInFromMenu() {
        actionwords.clickSignInFromMenu();
    }

    @When("^enter username: \"([^\"]*)\"$")
    public void enterUsernameP1(String username) {
        actionwords.enterUsernameP1(username);
    }

    @When("^enter password: \"([^\"]*)\"$")
    public void enterPasswordP1(String password) {
        actionwords.enterPasswordP1(password);
    }

    @When("click my profile icon")
    public void clickMyProfileIcon() {
        actionwords.clickMyProfileIcon();
    }

    @When("click sign out button")
    public void clickSignOutButton() {
        actionwords.clickSignOutButton();
    }

    @Then("Logout successful")
    public void logoutSuccessful() {
        actionwords.logoutSuccessful();
    }

}
