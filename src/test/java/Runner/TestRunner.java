package Runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="./src/main/resources/Login.feature",
        glue={"StepDefinitions"},
        monochrome = true,
        plugin = {"pretty","json:target/surefire-reports/report.json", "html:target/surefire-reports/report.html"}
)
public class TestRunner {
}

