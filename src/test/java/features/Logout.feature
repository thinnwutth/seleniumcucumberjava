Feature: Logout


  Scenario Outline: Verify logout functionality
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: "<username>"
    And enter password: "<password>"
    And click Sign In
    Then Login successful
    When click my profile icon
    And click sign out button
    Then Logout successful

    Examples:
      | username | password |
      | thinwutthmone@yomabank.com | f+1UzUFrqtBsUATFZntgrw== |

  @Automate @JIRA-YMQA-12 @JIRA-YMQA-65 @JIRA-YMQA-67 @JIRA-YMQA-69 @JIRA-YMQA-70
  Scenario Outline: Verify login functionality
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: "<username>"
    And enter password: "<password>"
    And click Sign In
    Then Login successful

    Examples:
      | username | password |
      | thinnwutth@gmail.com | f+1UzUFrqtBsUATFZntgrw== |
