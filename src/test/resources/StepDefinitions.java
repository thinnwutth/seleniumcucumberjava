package StepDefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;

public class StepDefinitions {
    public Actionwords actionwords = new Actionwords();

    @Given("launch GitHub web-based application")
    public void launchGitHubWebbasedApplication() {
        actionwords.launchGitHubWebbasedApplication();
    }

    @Then("Login successful")
    public void loginSuccessful(String freeText) {
        actionwords.loginSuccessful(freeText);
    }

    @When("click Sign In")
    public void clickSignIn() {
        actionwords.clickSignIn();
    }

    @When("click Sign In from menu")
    public void clickSignInFromMenu() {
        actionwords.clickSignInFromMenu();
    }

    @When("enter username: {string}")
    public void enterUsernameP1(String p1) {
        actionwords.enterUsernameP1(p1);
    }

    @When("enter password: {string}")
    public void enterPasswordP1(String p1) {
        actionwords.enterPasswordP1(p1);
    }

    @When("click my profile icon")
    public void clickMyProfileIcon() {
        actionwords.clickMyProfileIcon();
    }

    @When("click sign out button")
    public void clickSignOutButton() {
        actionwords.clickSignOutButton();
    }

    @Then("Logout successful")
    public void logoutSuccessful() {
        actionwords.logoutSuccessful();
    }
}